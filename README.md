# xample_hfd_sr_optimisation

This repository contains an example how to study detuning with amplitude in the presence of synchrontron radiation.




## Getting started

First, a Python 3.9 environment is required.
Then install the required python packages by running 

`python -m pip install -r python_requirements.txt`.

This will install the cpymad packages, together with some small other packages to run the example and tests.


## Run example

To run and play with the example, run 

`jupyter notebook`

to open a jupyter instance. 

Alternatively, the script can be run on SWAN, provided a CERN account.
<a href="https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/mihofer/xample_hfd_sr_optimisation.git" target="_blank">
  <img alt="" src="https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png">
</a>

Then open the `track.ipynb` example notebook to see how to set up a study.
