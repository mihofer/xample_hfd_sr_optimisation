
import json
import matplotlib.pyplot as plt

from pathlib import Path
from cpymad.madx import Madx
import xobjects as xo
import xpart as xp
import xdeps as xd
import xtrack as xt
from xtrack.slicing import Teapot, Strategy
from xtrack.twiss import TwissInit

OPERATION_MODE = 'z'

#  Load thick Mad-X lattice and convert to Xtrack line

with Madx() as madx:
    madx.call(f'./lattices/{OPERATION_MODE}/fccee_{OPERATION_MODE}.seq')
    madx.command.beam()
    madx.use('FCCEE_P_RING')
    madx.call(f'./lattices/install_matching_markers.madx')

    line = xt.Line.from_madx_sequence(
        madx.sequence['FCCEE_P_RING'],
        allow_thick=True,
        deferred_expressions=True,
    )

# Save to json

with open(f'fccee_{OPERATION_MODE}.json', 'w', encoding='utf-8') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)


# define strategy for elements and perform slicing

slicing_strategies = [
    Strategy(slicing=Teapot(10)),  # Default catch-all as in MAD-X
    Strategy(slicing=Teapot(10), element_type=xt.Bend),
    Strategy(slicing=Teapot(10), element_type=xt.Quadrupole),
    Strategy(slicing=Teapot(10), element_type=xt.Sextupole),
    Strategy(slicing=Teapot(10), element_type=xt.Multipole),
    Strategy(slicing=Teapot(25), name=r'^qd0.*'),
    Strategy(slicing=Teapot(25), name=r'^qf1.*'),
    Strategy(slicing=Teapot(25), name=r'^qf2.*'),
    Strategy(slicing=Teapot(25), name=r'^qd2.*'),
]

line.discard_tracker()
print("Slicing thick elements...")
line.slice_thick_elements(slicing_strategies)

print("Building tracker...")
line.build_tracker()


with open(f'fccee_{OPERATION_MODE}_thin.json', 'w', encoding='utf-8') as fid:
    json.dump(line.to_dict(), fid, cls=xo.JEncoder)