import xtrack as xt
import itertools
from xtrack.twiss import TwissInit

DEFAULT_TOL = {None: 1e-8, 'betx': 1e-6, 'bety': 1e-6} # to have no rematching w.r.t. madx

class TargetRMatrix(xt.Target):

    def __init__(self, tar, value, at_1=None, at_0=None, tag='',  **kwargs):

        super().__init__(tar=self.compute, value=value, tag=tag, **kwargs)

        assert tar in list(itertools.product(range(0,6), repeat=2)), 'Has to be a tuple of two indices, e.g. (1,1)'
        self.var = tar
        if at_1 is None:
            at_1 = '__ele_stop__'
        if at_0 is None:
            at_0 = '__ele_start__'
        self.at_1 = at_1
        self.at_0 = at_0

    def __repr__(self):
        return f'TargetR({self.var}({self.at_1}) - {self.var}({self.at_0}), value={self.value}, tol={self.tol}, weight={self.weight})'

    def compute(self, tw):

        rmatrix = tw.get_R_matrix(self.at_0, self.at_1)

        return rmatrix[self.var]


def rematch_arcuu(line, matching_param):

    result=line.match(
        solve=False,
        assert_within_tol=False,
        ele_start='s.arc_uu',
        ele_stop='e.arc_uu',
        twiss_init='periodic',
        method='4d',
        verbose=True,
        default_tol=DEFAULT_TOL,
        vary=[
            xt.Vary(name='kqd1', step=1e-10),
            xt.Vary(name='kqf2', step=1e-10),
            xt.Vary(name='kqd3', step=1e-10),
            xt.Vary(name='kqf4', step=1e-10),
            xt.Vary(name='kqd5', step=1e-10),
            xt.Vary(name='kqf6', step=1e-10),
        ],
        targets=[
            xt.Target('bety', value=matching_param['bysd1'], at='qd3a:19', tol=1e-4),
            xt.Target('betx', value=matching_param['bxsf1'], at='qf4a:19', tol=1e-4),
            xt.Target('bety', value=matching_param['bysd2'], at='qd5a:19', tol=1e-4),
            xt.Target('betx', value=matching_param['bxsf2'], at='qf2a:19', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=0, at='qf6a:9', tol=1e-4),
            

            xt.TargetInequality('bety', ineq_sign='>', rhs=matching_param['byend'], at='e.arc_uu', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=0, at='e.arc_uu', tol=1e-4),
            xt.Target('mux', value=2*matching_param['muxu'], at='e.arc_uu', tol=1e-4),
            xt.Target('muy', value=2*matching_param['muyu'], at='e.arc_uu', tol=1e-4),
        ],
    )
    return result

def rematch_arcussu(line, matching_param):
    cell_twiss = line.twiss(
    ele_start='s.arc_uu',
    ele_stop='e.arc_uu',
    twiss_init='periodic',
    method='4d',
    )
    
    result=line.match(
        solve=False,
        assert_within_tol=False,        
        ele_start='s.arc_ussu',
        ele_stop='e.arc_ussu',
        twiss_init=TwissInit(
            betx=cell_twiss['betx', -1],
            bety=cell_twiss['bety', -1],
            dx=cell_twiss['dx', -1],
        ),
        method='4d',
        verbose=True,
        default_tol=DEFAULT_TOL,
        vary=[
            xt.VaryList([
   'kqfs0', 'kqds0', 'kqfs1', 'kqds2', 'kqfs3', 'kqds4', 'kqfs5', 'kqdl1', 'kqfl2', 'kqdl2', 'kqfl3', 'kqdl3', 'kqfl6', 'kqdl4'], step=1e-10),
        ],
        targets=[
            xt.TargetInequality('betx', ineq_sign='<', rhs=140, at='qfs1a', tol=1e-3),

            xt.TargetInequality('betx', ineq_sign='<', rhs=140, at='qfs3a', tol=1e-3),
            xt.TargetInequality('bety', ineq_sign='>', rhs=25, at='qfs3a', tol=1e-3),

            xt.TargetInequality('betx', ineq_sign='<', rhs=190, at='qfs5a', tol=1e-3),
            xt.TargetInequality('bety', ineq_sign='>', rhs=25, at='qfs5a', tol=1e-3),

            xt.TargetInequality('betx', ineq_sign='<', rhs=260, at='qfl2a', tol=1e-3),

            xt.TargetInequality('betx', ineq_sign='<', rhs=matching_param['bxmax'], at='qfl3a', tol=1e-3),
            xt.TargetInequality('bety', ineq_sign='>', rhs=62, at='qfl3a', tol=1e-3),

            xt.TargetInequality('betx', ineq_sign='<', rhs=matching_param['bxmax'], at='qfl6a', tol=1e-3),

            xt.TargetInequality('betx', ineq_sign='<', rhs=matching_param['bxmax'], at='qfl4a', tol=1e-3),
            xt.TargetInequality('bety', ineq_sign='>', rhs=65, at='qfl4a', tol=1e-3),

            xt.TargetInequality('betx', ineq_sign='<', rhs=matching_param['bxmax'], at='qfl5a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=155, at='qds0a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=155, at='qds2a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=155, at='qds4a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=235, at='qdl1a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=matching_param['bymax'], at='qdl2a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=matching_param['bymax'], at='qdl3a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=matching_param['bymax'], at='qdl4a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=matching_param['bymax'], at='qdl5a', tol=1e-3),

            xt.TargetInequality('bety', ineq_sign='<', rhs=matching_param['bymax'], at='qdl5a:1', tol=1e-3),

            xt.Target('betx', value=matching_param['bxsc'], at='scenter', tol=1e-4),
            xt.Target('bety', value=matching_param['bysc'], at='scenter', tol=1e-4),
            xt.Target('dx', value=0, at='scenter', tol=1e-4),


            xt.Target('betx', value=cell_twiss['betx', -1], at='e.arc_ussu', tol=1e-4),
            xt.Target('bety', value=cell_twiss['bety', -1], at='e.arc_ussu', tol=1e-4),

            xt.Target('alfx', value=0.0, at='e.arc_ussu', tol=1e-4),
            xt.Target('alfy', value=0.0, at='e.arc_ussu', tol=1e-4),

            xt.Target('mux', value=cell_twiss['mux', -1]*0.5+3, at='e.arc_ussu', tol=1e-4),
            xt.Target('muy', value=cell_twiss['muy', -1]*0.5+3, at='e.arc_ussu', tol=1e-4),

            xt.Target('dx', value=cell_twiss['dx', -1], at='e.arc_ussu', tol=1e-4),
            xt.Target('dpx', value=0.0, at='e.arc_ussu', tol=1e-4),
        ],
    )
    return result

# to be checked
# def rematch_arcufl(line, matching_param):
#     cell_twiss = line.twiss(
#     ele_start='s.arc_uu',
#     ele_stop='e.arc_uu',
#     twiss_init='periodic',
#     method='4d',
#     )
    
#     result=line.match(
#         solve=False,
#         assert_within_tol=False,
#         ele_start='s.arc_uffu',
#         ele_stop='ccs_xl.d',
#         twiss_init=TwissInit(
#             betx=cell_twiss['betx', -1],
#             bety=cell_twiss['bety', -1],
#             dx=cell_twiss['dx', -1],
#         ),
#         method='4d',
#         verbose=True,
#         default_tol=DEFAULT_TOL,
#         vary=[
#             xt.VaryList([
#                 'kqfm1l', 'kqdm2l', 'kqfm3l', 'kqdm4l', 'kqfm5l', 'kqdm6l', 'kqfm7l', 'kqdm8l', 
#             ], step=1e-10),
#         ],
#         targets=[
#             xt.Target('betx', value=matching_param['bx_ff_in'], at='scrabl_exit:0', tol=1e-4),
#             xt.Target('bety', value=matching_param['by_ff_in'], at='scrabl_exit:0', tol=1e-4),
#             xt.Target('alfx', value=0.0, at='scrabl_exit:0', tol=1e-4),
#             xt.Target('alfy', value=0.0, at='scrabl_exit:0', tol=1e-4),            
#             xt.Target('mux', value=cell_twiss['mux', -1]*1.25+0.5, at='scrabl_exit:0', tol=1e-4),
#             xt.Target('muy', value=cell_twiss['muy', -1]*1.25+0.25, at='scrabl_exit:0', tol=1e-4),
#             xt.Target('dx', value=matching_param['dx_ff_in'], at='scrabl_exit:0', tol=1e-4),
#             xt.Target('dpx', value=0.0, at='scrabl_exit:0', tol=1e-4),

#         ],
#     )
#     return result

# def rematch_arcufr(line, matching_param):
#     cell_twiss = line.twiss(
#     ele_start='s.arc_uu',
#     ele_stop='e.arc_uu',
#     twiss_init='periodic',
#     method='4d',
#     )

#     result=line.match(
#         solve=False,
#         assert_within_tol=False,
#         ele_start='ccs_xr.d',
#         ele_stop='e.arc_uffu',
#         twiss_init=TwissInit(
#             betx=matching_param['bx_ff_in'],
#             bety=matching_param['by_ff_in'],
#             dx=matching_param['dx_ff_in'],
#         ),
#         method='4d',
#         verbose=True,
#         default_tol=DEFAULT_TOL,
#         vary=[
#             xt.VaryList([
#                 'kqfm1r', 'kqdm2r', 'kqfm3r', 'kqdm4r', 'kqfm5r', 'kqdm6r', 'kqfm7r', 'kqdm8r', 
#             ], step=1e-10),
#         ],
#         targets=[
            
#             xt.Target('betx', value=cell_twiss['betx', -1], at='e.arc_uffu', tol=1e-4),
#             xt.Target('bety', value=cell_twiss['bety', -1], at='e.arc_uffu', tol=1e-4),
#             xt.Target('alfx', value=0.0, at='e.arc_uffu', tol=1e-4),
#             xt.Target('alfy', value=0.0, at='e.arc_uffu', tol=1e-4),
#             xt.Target('mux', value=cell_twiss['mux', -1]*1.25+0.5, at='e.arc_uffu', tol=1e-4),
#             xt.Target('muy', value=cell_twiss['muy', -1]*1.25+0.25, at='e.arc_uffu', tol=1e-4),
#             xt.Target('dx', value=cell_twiss['dx', -1], at='e.arc_uffu', tol=1e-4),
#             xt.Target('dpx', value=0.0, at='e.arc_uffu', tol=1e-4),
#         ],
#     )
#     return result

def rematch_mccs_yl(line,matching_param):

    result=line.match(
        solve=False,
        assert_within_tol=False,
        ele_start='ccs_yl.d',
        ele_stop='ipd',
        twiss_init=TwissInit(
            betx=matching_param['bxip'],
            bety=matching_param['byip'],
            element_name='ipd'
        ),
        method='4d',
        verbose=True,
        default_tol=DEFAULT_TOL,
        vary=[
            xt.VaryList([
                'kqd0al', 'kqf1al', 'kqy01l', 'kqy02l', 'kqy03l', 'kqy04l', 
                'kqf02l', 'kqd02l', 'kqd04l', 'kqf05l', 'kqd06l', 
            ], step=1e-10),
        ],
        targets=[
            xt.TargetInequality('betx', ineq_sign='<', rhs=90.0, at='ipimag1:0', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=40.0, at='ipimag1:0', tol=1e-4),
            xt.TargetInequality('alfx', ineq_sign='>', rhs=-20.0, at='ipimag1:0', tol=1e-4),
            xt.Target('alfy', value=-1.1, at='ipimag1:0', tol=1e-4),

            xt.Target('bety', value=matching_param['bysdfl'], at='sdy1l_exit:0', tol=1e-4),
            xt.TargetInequality('betx', ineq_sign='<', rhs=120.0, at='sdy1l_exit:0', tol=1e-4),
            xt.Target('alfy', value=0.0, at='sdy1l_exit:0', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=0.25, at='sdy1l_exit:0', tol=1e-4),
            xt.Target('muy', value=-(0.75+matching_param['dmuy_sdy1l']), at='sdy1l_exit:0', tol=1e-4),
            
            xt.Target('betx', value=239.0, at='ipimag2:0', tol=1e-4),
            xt.Target('alfx', value=0.0, at='ipimag2:0', tol=1e-4),
            xt.Target('alfy', value=0.0, at='ipimag2:0', tol=1e-4),
            xt.Target('dx', value=0.3, at='ipimag2:0', tol=1e-4),
            xt.Target('dpx', value=matching_param['dpx_ccyl'], at='ipimag2:0', tol=1e-4),

            TargetRMatrix((0,1), matching_param['r12_ccsyl'], at_1='sdy1l_exit:0', at_0='sdy2l_exit:0', tol=1e-4),
            TargetRMatrix((2,3), matching_param['r34_ccsyl'], at_1='sdy1l_exit:0', at_0='sdy2l_exit:0', tol=1e-4),
            TargetRMatrix((2,2), -1, at_1='sdy1l_exit:0', at_0='sdy2l_exit:0', tol=1e-4),
            TargetRMatrix((3,2), 0.0, at_1='sdy1l_exit:0', at_0='sdy2l_exit:0', tol=1e-4),

#   RMATRIX, SDY1L[2]/SDY2L[2], RM(1,2) = R12_CCSy, RM(3,4) = R34_CCSy, & !to compensate the long sext aberr
#                               RM(3,3) = -1.0, &
#                               RM(4,3) =  0.0
        ],
    )
    return result

def rematch_mccs_yr(line,matching_param):

    result=line.match(
        solve=False,
        assert_within_tol=False,
        ele_start='ipd',
        ele_stop='ccs_yr.d',
        twiss_init=TwissInit(
            betx=matching_param['bxip'],
            bety=matching_param['byip'],
            element_name='ipd'
        ),
        method='4d',
        verbose=True,
        default_tol=DEFAULT_TOL,
        vary=[
            xt.VaryList([
                'kqd0ar', 'kqf1ar', 'kqy01r', 'kqy02r', 'kqy03r', 'kqy04r', 
                'kqf02r', 'kqd02r', 'kqd04r', 'kqf05r', 'kqd06r', 
            ], step=1e-10),
        ],
        targets=[
            xt.TargetInequality('betx', ineq_sign='<', rhs=90.0, at='ipimag1:1', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=40.0, at='ipimag1:1', tol=1e-4),
            xt.TargetInequality('alfx', ineq_sign='>', rhs=-20.0, at='ipimag1:1', tol=1e-4),
            xt.Target('alfy', value=-1.2, at='ipimag1:1', tol=1e-4),

            xt.Target('bety', value=matching_param['bysdfr'], at='sdy1r_exit:4', tol=1e-4),
            xt.TargetInequality('betx', ineq_sign='<', rhs=120.0, at='sdy1r_exit:4', tol=1e-4),
            xt.Target('alfy', value=0.0, at='sdy1r_exit:4', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=0.05, at='sdy1r_exit:4', tol=1e-4),
            xt.Target('muy', value=-(0.75+matching_param['dmuy_sdy1r']), at='sdy1r_exit:4', tol=1e-4),
            
            xt.Target('betx', value=135.0, at='ipimag2:0', tol=1e-4),
            xt.Target('alfx', value=0.0, at='ipimag2:0', tol=1e-4),
            xt.Target('alfy', value=0.0, at='ipimag2:0', tol=1e-4),
            xt.Target('dx', value=0.48, at='ipimag2:0', tol=1e-4),
            xt.Target('dpx', value=matching_param['dpx_ccyr'], at='ipimag2:0', tol=1e-4),

            TargetRMatrix((0,1), matching_param['r12_ccsyr'], at_1='sdy1r_exit:4', at_0='sdy2r_exit:4', tol=1e-4),
            TargetRMatrix((2,3), matching_param['r34_ccsyr'], at_1='sdy1r_exit:4', at_0='sdy2r_exit:4', tol=1e-4),
            TargetRMatrix((2,2), -1, at_1='sdy1r_exit:4', at_0='sdy2r_exit:4', tol=1e-4),
            TargetRMatrix((3,2), 0.0, at_1='sdy1r_exit:4', at_0='sdy2r_exit:4', tol=1e-4),

#   RMATRIX, SDY1L[2]/SDY2L[2], RM(1,2) = R12_CCSy, RM(3,4) = R34_CCSy, & !to compensate the long sext aberr
#                               RM(3,3) = -1.0, &
#                               RM(4,3) =  0.0
        ],
    )
    return result

def rematch_mccs_yxl(line, matching_param):

    result=line.match(
        solve=False,
        assert_within_tol=False,
        ele_start='ccs_xl.d',
        ele_stop='ipd',
        twiss_init=TwissInit(
            betx=matching_param['bxip'],
            bety=matching_param['byip'],
            element_name='ipd',
        ),
        method='4d',
        verbose=True,
        default_tol=DEFAULT_TOL,
        vary=[
            xt.VaryList([
                'kqd07l', 'kqf08l', 'kqd09l', 'kqf10l', 'kqd11l', 'kqf12l', 
                'kqx00l', 'kqx01l', 'kqx02l', 
                'kqf13l', 'kqd14l', 'kqf15l', 'kqd16l', 'kqf17l', 'kqd18l', 'kqf19l', 'kqd20l', 
            ], step=1e-10),
        ],
        targets=[
            xt.TargetInequality('betx', ineq_sign='<', rhs=75.0, at='ipimag3:0', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=20.0, at='ipimag3:0', tol=1e-4),
            xt.TargetInequality('alfx', ineq_sign='>', rhs=0.0, at='ipimag3:0', tol=1e-4),
            xt.TargetInequality('alfy', ineq_sign='>', rhs=-0.90, at='ipimag3:0', tol=1e-4),
            xt.TargetInequality('alfy', ineq_sign='<', rhs=0.9, at='ipimag3:0', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=matching_param['dx_sfml'], at='ipimag3:0', tol=1e-4),
            
            xt.TargetInequality('betx', ineq_sign='>', rhs=matching_param['bxsffl']*0.8, at='sfx1l_exit:0', tol=1e-4),
            xt.Target('alfx', value=0.0, at='sfx1l_exit:0', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=25, at='sfx1l_exit:0', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='<', rhs=0.6, at='sfx1l_exit:0', tol=1e-4),
            xt.Target('mux', value=1.75+matching_param['dmux_sfx1l'], at='sfx1l_exit:0', tol=1e-4),

#   RMATRIX, SFX1L[2]/SFX2L[2], RM(1,2) = R12_CCSx, RM(1,1)  =-1.0,&
#                               RM(3,4) = R34_CCSx!, RM(3,3) = -1.0
            TargetRMatrix((0,0), -1, at_0='sfx2l_exit:0', at_1='sfx1l_exit:0', tol=1e-4),
            TargetRMatrix((0,1), matching_param['r12_ccsx'], at_0='sfx2l_exit:0', at_1='sfx1l_exit:0', tol=1e-4),
            # TargetRMatrix((2,2), -1, at_0='sfx2l_exit:0', at_1='sfx1l_exit:0', tol=1e-4),
            TargetRMatrix((2,3), matching_param['r34_ccsx'], at_0='sfx2l_exit:0', at_1='sfx1l_exit:0', tol=1e-4),

            xt.TargetInequality('bety', ineq_sign='>', rhs=0.5, at='ipimag4:0', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=25.0, at='ipimag4:0', tol=1e-4),
            xt.Target('alfx', value=0.0, at='ipimag4:0', tol=1e-4),
            xt.Target('alfy', value=matching_param['delta_alfy_ccsx'], at='ipimag4:0', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=matching_param['dx_ccxl'], at='ipimag4:0', tol=1e-4),
            xt.Target('dpx', value=matching_param['dpx_ccxl'], at='ipimag4:0', tol=1e-4),

#   RMATRIX, #S/#E, RM(3,3)=-0.0, RM(1,2)=0.0 
            TargetRMatrix((0,1), 0.0, at_0='ccs_xl.d', at_1='ipd', tol=1e-4),
            TargetRMatrix((2,2), 0.0, at_0='ccs_xl.d', at_1='ipd', tol=1e-4),


            xt.Target('betx', value=matching_param['bx_ff_out'], at='ccs_xl.d', tol=1e-4),
            xt.Target('bety', value=matching_param['by_ff_out'], at='ccs_xl.d', tol=1e-4),
            xt.Target('alfx', value=0.0, at='ccs_xl.d', tol=1e-4),
            xt.Target('alfy', value=0.0, at='ccs_xl.d', tol=1e-4),
            xt.Target('dx', value=matching_param['dx_ff_out'], at='ccs_xl.d', tol=1e-4),
            xt.Target('dpx', value=0.0, at='ccs_xl.d', tol=1e-4),
            xt.Target('mux', value=-(3.0), at='ccs_xl.d', tol=1e-4),
            xt.Target('muy', value=-(2.75), at='ccs_xl.d', tol=1e-4),
        ],
    )
    return result

def rematch_mccs_yxr(line, matching_param):

    result=line.match(
        solve=False,
        assert_within_tol=False,
        ele_start='ipd',
        ele_stop='ccs_xr.d',
        twiss_init=TwissInit(
            betx=matching_param['bxip'],
            bety=matching_param['byip'],
            element_name='ipd',
        ),
        method='4d',
        verbose=True,
        default_tol=DEFAULT_TOL,
        vary=[
            xt.VaryList([
                'kqd07r', 'kqf08r', 'kqd09r', 'kqf10r', 'kqd11r', 'kqf12r', 
                'kqx00r', 'kqx01r', 'kqx02r', 
                'kqf13r', 'kqd14r', 'kqf15r', 'kqd16r', 'kqf17r', 'kqd18r', 'kqf19r', 'kqd20r', 
            ], step=1e-10),
        ],
        targets=[
            xt.TargetInequality('betx', ineq_sign='<', rhs=85.0, at='ipimag3:1', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=55.0, at='ipimag3:1', tol=1e-4),
            xt.Target('alfx', value=0.0, at='ipimag3:1', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=matching_param['dx_sfmr'], at='ipimag3:1', tol=1e-4),
            
            xt.TargetInequality('betx', ineq_sign='>', rhs=matching_param['bxsffr']*0.8, at='sfx1r_exit:4', tol=1e-4),
            xt.Target('alfx', value=0.0, at='sfx1r_exit:4', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=35, at='sfx1r_exit:4', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='<', rhs=1.2, at='sfx1r_exit:4', tol=1e-4),
            xt.Target('mux', value=1.75+matching_param['dmux_sfx1r'], at='sfx1r_exit:4', tol=1e-4),

#   RMATRIX, SFX1L[2]/SFX2L[2], RM(1,2) = R12_CCSx, RM(1,1)  =-1.0,&
#                               RM(3,4) = R34_CCSx!, RM(3,3) = -1.0
            TargetRMatrix((0,0), -1, at_0='sfx1r_exit:4', at_1='sfx2r_exit:4', tol=1e-4),
            TargetRMatrix((0,1), matching_param['r12_ccsx'], at_0='sfx1r_exit:4', at_1='sfx2r_exit:4', tol=1e-4),
            # TargetRMatrix((2,2), -1, at_0='sfx1r_exit:4', at_1='sfx2r_exit:4', tol=1e-4),
            TargetRMatrix((2,3), matching_param['r34_ccsx'], at_0='sfx1r_exit:4', at_1='sfx2r_exit:4', tol=1e-4),

            xt.TargetInequality('bety', ineq_sign='>', rhs=0.5, at='ipimag4:1', tol=1e-4),
            xt.TargetInequality('bety', ineq_sign='<', rhs=35.0, at='ipimag4:1', tol=1e-4),
            xt.Target('alfx', value=0.0, at='ipimag4:1', tol=1e-4),
            xt.Target('alfy', value=-matching_param['delta_alfy_ccsx'], at='ipimag4:1', tol=1e-4),
            xt.TargetInequality('dx', ineq_sign='>', rhs=matching_param['dx_ccxr'], at='ipimag4:1', tol=1e-4),
            xt.Target('dpx', value=matching_param['dpx_ccxr'], at='ipimag4:1', tol=1e-4),

#   RMATRIX, #S/#E, RM(3,3)=-0.0, RM(1,2)=0.0 
            TargetRMatrix((0,1), 0.0, at_0='ipd', at_1='ccs_xr.d', tol=1e-4),
            TargetRMatrix((2,2), 0.0, at_0='ipd', at_1='ccs_xr.d', tol=1e-4),


            xt.Target('betx', value=matching_param['bx_ff_out'], at='ccs_xr.d', tol=1e-4),
            xt.Target('bety', value=matching_param['by_ff_out'], at='ccs_xr.d', tol=1e-4),
            xt.Target('alfx', value=0.0, at='ccs_xr.d', tol=1e-4),
            xt.Target('alfy', value=0.0, at='ccs_xr.d', tol=1e-4),
            xt.Target('dx', value=matching_param['dx_ff_out'], at='ccs_xr.d', tol=1e-4),
            xt.Target('dpx', value=0.0, at='ccs_xr.d', tol=1e-4),
            xt.Target('mux', value=-(3.0), at='ccs_xr.d', tol=1e-4),
            xt.Target('muy', value=-(2.75), at='ccs_xr.d', tol=1e-4),
        ],
    )
    return result

def rematch_ccs_yxl(line):
    return


