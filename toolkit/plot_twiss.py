
import json
import matplotlib.pyplot as plt

from pathlib import Path
from cpymad.madx import Madx
import xobjects as xo
import xpart as xp
import xdeps as xd
import xtrack as xt
from xtrack.slicing import Teapot, Strategy
from xtrack.twiss import TwissInit

OPERATION_MODE = 'z'
REFERENCE_PARAMETER = json.load(open('./reference_parameters.json'))

#  Load thick Mad-X lattice and convert to Xtrack line

if Path(f'fccee_{OPERATION_MODE}.json').exists():
    with open(f'fccee_{OPERATION_MODE}.json', 'r', encoding='utf-8') as fid:
        loaded_dct = json.load(fid)

    line = xt.Line.from_dict(loaded_dct)
else:
    print('Line thick not found.')
    exit()

if Path(f'fccee_{OPERATION_MODE}_thin.json').exists():
    with open(f'fccee_{OPERATION_MODE}_thin.json', 'r', encoding='utf-8') as fid:
        loaded_dct = json.load(fid)

    line_thin = xt.Line.from_dict(loaded_dct)
else:
    print('Line thin not found.')
    exit()

# build tracker for thick line, run twiss and check optics
ref_particle = xp.Particles(
        mass0=xp.ELECTRON_MASS_EV,
        q0=1,
        p0c=REFERENCE_PARAMETER[OPERATION_MODE]['ENERGY']*1e9,
        x=0,
        y=0
        )
line.particle_ref = ref_particle
line_thin.particle_ref = ref_particle

line.build_tracker()
thick_twiss = line.twiss(method='4d').to_pandas()

line_thin.build_tracker()
thin_twiss = line_thin.twiss(method='4d').to_pandas()


fig, ax = plt.subplots(nrows=2, ncols=1, figsize=(9,9), constrained_layout=True, sharex=True)

ax[0].plot(thick_twiss['s'], thick_twiss['betx'], color='red')
ax[0].plot(thick_twiss['s'], thick_twiss['bety'], color='blue')

ax[0].plot(thin_twiss['s'], thin_twiss['betx'], color='red', linestyle='--')
ax[0].plot(thin_twiss['s'], thin_twiss['bety'], color='blue', linestyle='--')

for idx, data in thick_twiss.loc[thick_twiss['name'].str.contains('^ip|^ccs|^s\.|^e\.')].iterrows():
    ax[0].axvline(data['s'], color='black')


ax[1].plot(thick_twiss['s'], thick_twiss['dx'], color='lime')
ax[1].plot(thin_twiss['s'], thin_twiss['dx'], color='lime')

plt.show()